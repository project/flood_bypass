CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module helps in login attempt as many times as required, because 
this module bypasses the flood service of drupal. So that a user 
may attempt unlimited tries to login in drupal.

This module is best suited in cases where there is a requirement to 
bypass flood system of Drupal.

 * For a full description of the module visit:
   https://www.drupal.org/project/flood_bypass

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/flood_bypass


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Flood ByPass module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Flood ByPass
       module.


MAINTAINERS
-----------

 * Harsh Behl (pen) - https://www.drupal.org/u/pen

Supporting organization:

 * TO THE NEW - https://www.drupal.org/to-the-new

TO THE NEW is a digital technology company that builds disruptive products and transforms businesses. Our Services include Drupal Development, Drupal Migration, Drupal Upgrade, Team Augmentation.
